#include <CoOS.h>			              /*!< CoOS header file	         */
#include <stdio.h>
#include "stm32f4xx_conf.h"
#include "LCD.h"
#include "LedTask.h"
#include "BSP.h"



#define STACK_SIZE_LCD 1024              /*!< Define "taskA" task size */
OS_STK     LCD_stk[2][STACK_SIZE_LCD];	  /*!< Define "taskA" task stack */




void LCDManagerTask (void* pdata);
void LCDHelloWorldTask(void * parg);
void LCDGradientTask(void * parg);
void LCDDrawAreaTask(void * parg);
void LCDScopeTask(void * parg);

#define STACK_SIZE_LCD 1024              /*!< Define "taskA" task size */
OS_STK     LCD_stk[2][STACK_SIZE_LCD];	  /*!< Define "taskA" task stack */

void CreateLCDTask(void){

	Init_AnalogJoy();

	LCD_Initialization();
	LCD_Clear(Blue);

	CoCreateTask (LCDManagerTask,0,1,&LCD_stk[0][STACK_SIZE_LCD-1],STACK_SIZE_LCD);

}

void LCDManagerTask (void* pdata) {

	OS_TID lcdId;


  for (;;) {

	  lcdId =	CoCreateTask (LCDHelloWorldTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);

	  lcdId =	CoCreateTask (LCDGradientTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);

	  lcdId =	CoCreateTask (LCDDrawAreaTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);

	  lcdId =	CoCreateTask (LCDScopeTask,0,1,&LCD_stk[1][STACK_SIZE_LCD-1],STACK_SIZE_LCD);
	  waitForKey(5,0);
	  CoDelTask(lcdId);


  }
}



void LCDHelloWorldTask(void * parg){
	char str[32];
	uint16_t i=0;
	LCD_Clear(Red);
	LCD_PrintText(10,20, "Hola Mundo!", Blue, White);


	for(;;){
		sprintf(str,"Variable i: %d", i);
		LCD_PrintText(10,32,str,Blue,White);
		i++;
		CoTimeDelay(0,0,0,500);
	}

}

void LCDGradientTask(void * parg){


	LCD_Clear(Black);
	LCD_PrintText(10,224,"LCDGradientTask",White,Blue);


	for(;;){

		CoTimeDelay(0,0,0,500);
	}

}



void LCDDrawAreaTask(void * parg){

	LCD_Clear(Black);
	LCD_PrintText(10,224,"LCDDrawAreaTask",White,Blue);

	for(;;){
		CoTimeDelay(0,0,0,5);


	}


}


void LCDScopeTask(void * parg){

	LCD_Clear(Blue);
	LCD_PrintText(10,224,"LCDScopeTask",White,Black);

	for(;;){
		CoTimeDelay(0,0,0,10);

	}


}
